# -*- coding: utf-8 -*-
from raices import *
from numpy.linalg import inv
import numpy as np
#from scipy import sparse

GL=np.array((Raiz()),dtype=float)
Wg=np.array((Wei()),dtype=float)
#_______________________________________________________________________________________________________________________
#_________________________________________________Módulos de funciones__________________________________________________
#_______________________________________________________________________________________________________________________

#_______________________________________________________________________________________________________________________
'''__________________________________MÓDULO 1: "FunMLS". Funciones propias del MLS___________________________________'''

'''1) Evaluar # de pts en el dominio de soporte'''
def Dsoporte(c,Dmax,ListaDePuntos):
 disx = []; disy = []; disz = []; orden = [] # se crean lista para asiganar valores.

 N = np.shape(ListaDePuntos)  # se calcula las demiensiones de la matriz de la lista de puntos.
 pun2 = np.zeros(N)           # se crea una matriz de 0 del tamaño de la matriz de la lista de puntos.
 dif2=np.zeros(N)            # se crea una matriz de 0 del tamaño de la matriz de lista de puntos.

 pun2[:][:,0] = c[0]      # se le da el valor de de c[0] a todos la posciones de la fila [0].
 pun2[:][:,1] = c[1]      # se le da el valor de de c[1] a todos la posciones de la fila [1].
 pun2[:][:,2] = c[2]      # se le da el valor de de c[2] a todos la posciones de la fila [2].


 dif = abs(pun2 - ListaDePuntos) # se calcula en absoluto la diferencia entre el el centro del D.S. y todos los nodos.
 dif3 = (pun2 - ListaDePuntos)   # se caclula la diferencia entre el centro del D.S. y todos los nodos.

 dif2[:][:, 0] = dif[:][:, 0] - Dmax[0] * np.ones(N[0]) # calcula diferencia entre la cordena en x del C.D.S y la cordenada x los nodos.
 dif2[:][:, 1] = dif[:][:, 1] - Dmax[1] * np.ones(N[0]) # calcula diferencia entre la cordena en y del C.D.S y la cordenada y los nodos. 
 dif2[:][:, 2] = dif[:][:, 2] - Dmax[2] * np.ones(N[0]) # calcula diferencia entre la cordena en z del C.D.S y la cordenada z los nodos.


 for i in range(N[0]): #ciclo para analisar cada nodos con el C.D.S.
   if (dif2[i][0] <= 0.000001) and (dif2[i][1] <= 0.000001) and (dif2[i][2] <= 0.000001): # condicion para saber si el nodo esta dentro o fuera del D.S.
     disx.append(dif3[i][0])   # valor de la cordenada en x de un nodo que se encuetra dentro del D.S.
     disy.append(dif3[i][1])   # valor de la cordenada en y de un nodo que se encuetra dentro del D.S.
     disz.append(dif3[i][2])   # valor de la cordenada en z de un nodo que se encuetra dentro del D.S.
     orden.append(i)           # nombre del nodo que se encuentra dentro del D.S.
 orden = np.array(orden,dtype=int)       # vuelve la lista un vector
 dis=np.array([disx,disy,disz],dtype=float) # vuelve la lista en matrix 
 return (orden, dis)
#_______________________________________________________________________________________________________________________


#_______________________________________________________________________________________________________________________
# 3) Función para calcular matriz de pesos
def wf(DominioS, dis,Dmax):
 d_s = np.shape(DominioS)
 numnodos = max(d_s)
 Ww = np.zeros((numnodos, numnodos), dtype=float)
 Wx = np.zeros((numnodos, numnodos), dtype=float)
 Wy = np.zeros((numnodos, numnodos), dtype=float)
 Wz = np.zeros((numnodos, numnodos), dtype=float)


 for i in range(numnodos):
   drdx = np.sign(dis[0,i]) / Dmax[0]
   drdy = np.sign(dis[1,i]) / Dmax[1]
   drdz = np.sign(dis[2,i]) / Dmax[2]
   rx = abs(dis[0,i] / Dmax[0])
   ry = abs(dis[1,i] / Dmax[1])
   rz = abs(dis[2,i] / Dmax[2])
   if rx > 0.5:
     wx = (4 / 3) - 4 * rx + 4 * rx * rx - (4 / 3) * rx ** 3
     dwx = (-4 + 8 * rx - 4 * rx ** 2) * drdx
   elif rx <= 0.5:
     wx = (2 / 3) - 4 * rx * rx + 4 * rx ** 3
     dwx = (-8 * rx + 12 * rx ** 2) * drdx

   if ry > 0.5:
     wy = (4 / 3) - 4 * ry + 4 * ry ** 2 - (4 / 3) * ry ** 3
     dwy = (-4 + 8 * ry - 4 * ry ** 2) * drdy
   elif ry <= 0.5:
     wy = (2 / 3) - 4 * ry ** 2 + 4 * ry ** 3
     dwy = (-8 * ry + 12 * ry ** 2) * drdy

   if rz > 0.5:
     wz = (4 / 3) - 4 * rz + 4 * rz ** 2 - (4 / 3) * rz ** 3
     dwz = (-4 + 8 * rz - 4 * rz ** 2) * drdz
   elif rz <= 0.5:
     wz = (2 / 3) - 4 * rz ** 2 + 4 * rz ** 3
     dwz = (-8 * rz + 12 * rz ** 2) * drdz
   Ww[i][i] = wx * wy * wz
   Wx[i][i] = dwx * wy * wz
   Wy[i][i] = wx * dwy * wz
   Wz[i][i] = wx * wy * dwz

 WW = np.asmatrix(Ww)
 WX = np.asmatrix(Wx)
 WY = np.asmatrix(Wy)
 WZ = np.asmatrix(Wz)
 return (WW, WX, WY, WZ)



#_______________________________________________________________________________
# 7) Cálculo de coeficientes a
def Coeficientesa(DominioS, c, dis,Dmax,m,Expo):
 P = CalcularMp(DominioS,m,Expo)
 [W, Wx, Wy, Wz] = wf(DominioS, dis,Dmax)
 PP = np.matrix(P)
 A = np.transpose(PP) * W * PP
 dAx = np.transpose(PP) * Wx * PP
 dAy = np.transpose(PP) * Wy * PP
 dAz = np.transpose(PP) * Wz * PP

 B = np.transpose(PP) * W
 dBx = np.transpose(PP) * Wx
 dBy = np.transpose(PP) * Wy
 dBz = np.transpose(PP) * Wz

 invA = inv(A)
 invAB = invA * B
 dAinvx = (-invA * dAx * invA)
 dAinvy = (-invA * dAy * invA)
 dAinvz = (-invA * dAz * invA)

 Pc = CalcularMp(c,m,Expo)

 dpx = DerivadaPx(c,m,Expo)
 dpy = DerivadaPy(c,m,Expo)
 dpz = DerivadaPz(c,m,Expo)

 invAdBx = invA * dBx
 invAdBy = invA * dBy
 invAdBz = invA * dBz

 dAinvxB = dAinvx * B
 dAinvyB = dAinvy * B
 dAinvzB = dAinvz * B

 dphyx = (np.transpose(dpx) * invAB) + Pc * (invAdBx + dAinvxB)
 dphyy = (np.transpose(dpy) * invAB) + Pc * (invAdBy + dAinvyB)
 dphyz = (np.transpose(dpz) * invAB) + Pc * (invAdBz + dAinvzB)

 prox = np.array(dphyx)
 proy = np.array(dphyy)
 proz = np.array(dphyz)
 return (prox, proy, proz)

#__________________________________________________________________________________
# 8) Cálculo de funciones basicas del MLS (Sol para un pto, osea phi)
def PHI(c,Dmax,m,Expo,ListaDePuntos):
 [Orden,dis]=Dsoporte(c,Dmax,ListaDePuntos)
 DominioS=np.transpose(np.array(ListaDePuntos[Orden],dtype=float))
 P=CalcularMp(DominioS,m,Expo)
 (W,Wx,Wy,Wz)=wf(DominioS,dis,Dmax)
 Pt=np.transpose(np.asmatrix(P))
 Pc = CalcularMp(c,m,Expo)
 M = Pt * W * P
 B=Pt*W
 A=inv(M)*(B)
 phi=np.asarray(Pc*A)
 numDS=len(Orden)
 return(phi,numDS,Orden)

#______________________________________________________FIN MÓDULO FunMLS________________________________________________
#_______________________________________________________________________________________________________________________
#__________________________________MÓDULO 2: "Funelas". Funciones de elasticidad________________________________________

#_______________________________________________________________________________________________________________________
# 2) Armado de strain matrix
def SM(prox, proy,proz):
    n = max(prox.shape)
    B = np.zeros((6, 3 * n))

    B[0][range(0,3*n,3)]=prox[0][:]
    B[1][range(1,3*n,3)]=proy[0][:]

    B[2][range(2,3*n,3)]=proz[0][:]
    B[3][range(1,3*n,3)]=proz[0][:]
    B[3][range(2,3*n,3)]=proy[0][:]
    B[4][range(0,3*n,3)]=proz[0][:]
    B[4][range(2,3*n,3)]=prox[0][:]
    B[5][range(0,3*n,3)]=proy[0][:]
    B[5][range(1,3*n,3)]=prox[0][:]
    return (B)

#_______________________________________________________________________________
# 3) Calculo de la matriz K
def CalculoK(PgD,D,N,Dmax,m,Expo,ListaDePuntos):
 cuenta=0
 NPdD = len(PgD)
 K = np.zeros((3 * N, 3 * N), dtype=float)

 for i in range(NPdD):
   c=PgD[i][0:3]
   jacobian=PgD[i][3]
   [B5, ORDEN] = integrandok(c,Dmax,m,Expo,ListaDePuntos)
   L1 = len(ORDEN)

   en = np.zeros((3 * L1), dtype=int)
   ORDEN = np.array(ORDEN)

   en[range(0, 3 * L1, 3)] = 3 * ORDEN
   en[range(1, 3 * L1, 3)] = 3 * ORDEN + 1
   en[range(2, 3 * L1, 3)] = 3 * ORDEN + 2

   B5=np.asmatrix(B5)
   B5t=np.transpose(B5)
   M_s=jacobian*np.linalg.multi_dot([B5t,D,B5])
   K[tuple(en[np.mgrid[:3*L1,:3*L1]])]+=M_s
   porcentaje=round(100*i/NPdD,1)
   if porcentaje>cuenta:
    print("Calculo de K en un:",round(100*i/NPdD,1),"%")
    cuenta=porcentaje+2
 return (K)


#______________________________________________________FIN MÓDULO Funelas_______________________________________________

#_______________________________________________________________________________________________________________________
# __________________________________MÓDULO 3: "Funinte". Funciones de integración________________________________________

#_______________________________________________________________________________________________________________________
# 2) Armado de matriz k, funcion dentro de la integral
def integrandok(c,Dmax,m,Expo,ListaDePuntos):
 [Orden, dis] = Dsoporte(c, Dmax,ListaDePuntos)
 DominioS = np.transpose(ListaDePuntos[Orden])
 [prox, proy, proz] = Coeficientesa(DominioS, c, dis,Dmax,m,Expo)
 B = SM(prox, proy, proz)
 return (B, Orden)

#_______________________________________________________________________________________________________________________




#_______________________________________________________________________________________________________________________
#_______________________________________________________________________________________________________________________
#_________________________________________MÓDULO 4: Posprocesamiento____________________________________________________

def posprocesamiento(SOL,D,N,Dmax,m,Expo,ListaDePuntos,ListaDePuntos2):
    u = []; sigmax = []; sigmay = []; sigmaz = []; tauyz = []; tauxz = []; tauxy = []
    
    for i in range(N):
        c = ListaDePuntos2[i]
        sol = []
        [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
        [sigma, vicky] = integrandok(c, Dmax, m, Expo, ListaDePuntos)
        PHIM = SM(phi, phi, phi)
        PHIM = np.delete(PHIM, (3, 4, 5), 0)
        
        sol = np.zeros((3 * numDS), dtype=float)
        sol[range(0, 3 * numDS, 3)] = SOL[3 * Orden, 0]
        sol[range(1, 3 * numDS, 3)] = SOL[3 * Orden + 1, 0]
        sol[range(2, 3 * numDS, 3)] = SOL[3 * Orden + 2, 0]

        u.append(np.transpose(PHIM.dot(sol)))
        esf = np.linalg.multi_dot([D, sigma, sol])
        esf = np.array(esf)

        sigmax.append(esf[0][0])
        sigmay.append(esf[0][1])
        sigmaz.append(esf[0][2])
        tauyz.append(esf[0][3])
        tauxz.append(esf[0][4])
        tauxy.append(esf[0][5])
        print("posprocesamiento en un:",round(100*i/N,1),"%")
    sigmax = np.array(sigmax)
    sigmay = np.array(sigmay)
    sigmaz = np.array(sigmaz)
    tauxy = np.array(tauxy)
    tauxz = np.array(tauxz)
    tauyz = np.array(tauyz)
    u = np.array(u)
    return (u,sigmax,sigmay,sigmaz,tauyz,tauxz,tauxy)
#_______________________________________________________________________________________________________________________
#_______________________________________________________Fin?____________________________________________________________
