""" Module to define helper functions """
import numpy as np
from numpy.linalg import inv


def variable_change(a, b, x):
    """
    Function para cambio de variables module: funciones de integracion
    original comment: # 1) Cambio de variables
    """
    y = (b - a) * x / 2 + (a + b) / 2
    dy = abs(b - a) / 2
    return y, dy


def Dsoporte(c, Dmax, ListaDePuntos):
    """
    '''1) Evaluar # de pts en el dominio de soporte'''
    Args:
        c:
        Dmax:
        ListaDePuntos:

    Returns:

    """
    disx = []
    disy = []
    disz = []
    orden = []  # se crean lista para asiganar valores.

    N = np.shape(ListaDePuntos)  # se calcula las demiensiones de la matriz de la lista de puntos.
    pun2 = np.zeros(N)  # se crea una matriz de 0 del tamaño de la matriz de la lista de puntos.
    dif2 = np.zeros(N)  # se crea una matriz de 0 del tamaño de la matriz de lista de puntos.

    pun2[:][:, 0] = c[0]  # se le da el valor de de c[0] a todos la posciones de la fila [0].
    pun2[:][:, 1] = c[1]  # se le da el valor de de c[1] a todos la posciones de la fila [1].
    pun2[:][:, 2] = c[2]  # se le da el valor de de c[2] a todos la posciones de la fila [2].

    dif = abs(
        pun2 - ListaDePuntos)  # se calcula en absoluto la diferencia entre el el centro del D.S. y todos los nodos.
    dif3 = (pun2 - ListaDePuntos)  # se caclula la diferencia entre el centro del D.S. y todos los nodos.

    dif2[:][:, 0] = dif[:][:, 0] - Dmax[0] * np.ones(
        N[0])  # calcula diferencia entre la cordena en x del C.D.S y la cordenada x los nodos.
    dif2[:][:, 1] = dif[:][:, 1] - Dmax[1] * np.ones(
        N[0])  # calcula diferencia entre la cordena en y del C.D.S y la cordenada y los nodos.
    dif2[:][:, 2] = dif[:][:, 2] - Dmax[2] * np.ones(
        N[0])  # calcula diferencia entre la cordena en z del C.D.S y la cordenada z los nodos.

    for i in range(N[0]):  # ciclo para analisar cada nodos con el C.D.S.
        if (dif2[i][0] <= 0.000001) and (dif2[i][1] <= 0.000001) and (
                dif2[i][2] <= 0.000001):  # condicion para saber si el nodo esta dentro o fuera del D.S.
            disx.append(dif3[i][0])  # valor de la cordenada en x de un nodo que se encuetra dentro del D.S.
            disy.append(dif3[i][1])  # valor de la cordenada en y de un nodo que se encuetra dentro del D.S.
            disz.append(dif3[i][2])  # valor de la cordenada en z de un nodo que se encuetra dentro del D.S.
            orden.append(i)  # nombre del nodo que se encuentra dentro del D.S.
    orden = np.array(orden, dtype=int)  # vuelve la lista un vector
    dis = np.array([disx, disy, disz], dtype=float)  # vuelve la lista en matrix
    return orden, dis


# 2) Función para calcular matriz P
def CalcularMp(DominioS, m, Expo):
    n = max(DominioS.shape)
    if n == 3:
        K = np.zeros((m), dtype=float)
        for j in range(m):
            K[j] = ((DominioS[0]) ** Expo[0, j]) * ((DominioS[1]) ** Expo[1, j]) * ((DominioS[2]) ** Expo[2, j])
    else:
        K = np.zeros((n, m), dtype=float)
        for i in range(n):
            x3 = DominioS[0][i]
            y3 = DominioS[1][i]
            z3 = DominioS[2][i]
            for j in range(m):
                K[i][j] = (x3 ** Expo[0, j]) * (y3 ** Expo[1, j]) * (z3 ** Expo[2, j])
    return K


def wf(DominioS, dis, Dmax):
    """
    # 3) Función para calcular matriz de pesos
    Args:
        DominioS:
        dis:
        Dmax:

    Returns:

    """
    d_s = np.shape(DominioS)
    numnodos = max(d_s)
    Ww = np.zeros((numnodos, numnodos), dtype=float)
    Wx = np.zeros((numnodos, numnodos), dtype=float)
    Wy = np.zeros((numnodos, numnodos), dtype=float)
    Wz = np.zeros((numnodos, numnodos), dtype=float)

    for i in range(numnodos):
        drdx = np.sign(dis[0, i]) / Dmax[0]
        drdy = np.sign(dis[1, i]) / Dmax[1]
        drdz = np.sign(dis[2, i]) / Dmax[2]
        rx = abs(dis[0, i] / Dmax[0])
        ry = abs(dis[1, i] / Dmax[1])
        rz = abs(dis[2, i] / Dmax[2])
        if rx > 0.5:
            wx = (4 / 3) - 4 * rx + 4 * rx * rx - (4 / 3) * rx ** 3
            dwx = (-4 + 8 * rx - 4 * rx ** 2) * drdx
        elif rx <= 0.5:
            wx = (2 / 3) - 4 * rx * rx + 4 * rx ** 3
            dwx = (-8 * rx + 12 * rx ** 2) * drdx

        if ry > 0.5:
            wy = (4 / 3) - 4 * ry + 4 * ry ** 2 - (4 / 3) * ry ** 3
            dwy = (-4 + 8 * ry - 4 * ry ** 2) * drdy
        elif ry <= 0.5:
            wy = (2 / 3) - 4 * ry ** 2 + 4 * ry ** 3
            dwy = (-8 * ry + 12 * ry ** 2) * drdy

        if rz > 0.5:
            wz = (4 / 3) - 4 * rz + 4 * rz ** 2 - (4 / 3) * rz ** 3
            dwz = (-4 + 8 * rz - 4 * rz ** 2) * drdz
        elif rz <= 0.5:
            wz = (2 / 3) - 4 * rz ** 2 + 4 * rz ** 3
            dwz = (-8 * rz + 12 * rz ** 2) * drdz
        Ww[i][i] = wx * wy * wz
        Wx[i][i] = dwx * wy * wz
        Wy[i][i] = wx * dwy * wz
        Wz[i][i] = wx * wy * dwz

    WW = np.asmatrix(Ww)
    WX = np.asmatrix(Wx)
    WY = np.asmatrix(Wy)
    WZ = np.asmatrix(Wz)
    return WW, WX, WY, WZ


def PHI(c, Dmax, m, Expo, ListaDePuntos):
    """
    # 8) Cálculo de funciones basicas del MLS (Sol para un pto, osea phi)
    Args:
        c:
        Dmax:
        m:
        Expo:
        ListaDePuntos:

    Returns:

    """
    [Orden, dis] = Dsoporte(c, Dmax, ListaDePuntos)
    DominioS = np.transpose(np.array(ListaDePuntos[Orden], dtype=float))
    P = CalcularMp(DominioS, m, Expo)
    (W, Wx, Wy, Wz) = wf(DominioS, dis, Dmax)
    Pt = np.transpose(np.asmatrix(P))
    Pc = CalcularMp(c, m, Expo)
    M = Pt * W * P
    B = Pt * W
    A = inv(M) * (B)
    phi = np.asarray(Pc * A)
    numDS = len(Orden)
    return phi, numDS, Orden


def Ensamblarphi(phi, Orden, N):
    """
    # 1) Ensamblar la matriz Phi
    Args:
        phi:
        Orden:
        N:

    Returns:

    """
    PHIs = np.zeros((3, 3 * N), dtype=float)
    PHIs[0][3 * Orden] = phi
    PHIs[1][3 * Orden + 1] = phi
    PHIs[2][3 * Orden + 2] = phi
    return PHIs


def SR(x, y, z, bb):
    # 3.0) Respuesta real.
    urx = 0  # (-bb[1] * y / (6 * E * Imo)) * ((6 * longitudx - 3 * x) * x + (2 + nu) * (y ** 2 - longitudy ** 2 / 4))
    ury = 0  # (bb[1] / (6 * E * Imo)) * (3 * nu * y ** 2 * (longitudx - x) + (4 + 5 * nu) * (x * (longitudy ** 2) / 4) + (3 * longitudx - x) * x ** 2)
    urz = 0  # (bb[1] / (6 * E * Imo)) * (3 * nu * z ** 2 * (longitudx - x) + (4 + 5 * nu) * (longitudz * (x ** 2) / 4) + (3 * longitudz - x) * x ** 2)

    # u2 = (-bb[1] * z / (6 * E * Imo)) * ((6 * longitudx - 3 * x) * x + (2 + nu) * (z ** 2 - longitudz ** 2 / 4))
    b = np.matrix([
        [0],
        [0],
        [
            bb[1],
        ]
    ])  # np.matrix([[0],[(bb[1]/(2*Imo))*(longitudy**2/4-y**2)],[0]])#[(bb[1]/(2*Imo))*(longitudz**2/4-z**2)]])
    ur = np.array([urx, ury, urz])
    return ur, b


# _______________________________________________________________________________
# 4) DEREVIDAD DE P  EN X
def DerivadaPx(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp1 = np.absolute((Expo[0, i] - 1))
        dp1.append(Expo[0, i] * (c[0] ** (expdp1)) * (c[1] ** (Expo[1, i])) * (c[2] ** (Expo[2, i])))
    return (dp1)


# _______________________________________________________________________________
# 5) DEREVIDAD DE P  EN Y
def DerivadaPy(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp2 = abs(Expo[1, i] - 1)
        dp1.append(Expo[1, i] * (c[0] ** (Expo[0, i])) * (c[1] ** (expdp2)) * c[2] ** (Expo[2, i]))
    return (dp1)


# _______________________________________________________________________________
# 6) DEREVIDAD DE P  EN Z
def DerivadaPz(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp3 = abs(Expo[2, i] - 1)
        dp1.append(Expo[2, i] * (c[0] ** (Expo[0, i])) * (c[1] ** (Expo[1, i])) * (c[2] ** (expdp3)))
    return (dp1)


# 7) Cálculo de coeficientes a
def Coeficientesa(DominioS, c, dis, Dmax, m, Expo):
    P = CalcularMp(DominioS, m, Expo)
    [W, Wx, Wy, Wz] = wf(DominioS, dis, Dmax)
    PP = np.matrix(P)
    A = np.transpose(PP) * W * PP
    dAx = np.transpose(PP) * Wx * PP
    dAy = np.transpose(PP) * Wy * PP
    dAz = np.transpose(PP) * Wz * PP

    B = np.transpose(PP) * W
    dBx = np.transpose(PP) * Wx
    dBy = np.transpose(PP) * Wy
    dBz = np.transpose(PP) * Wz

    invA = inv(A)
    invAB = invA * B
    dAinvx = (-invA * dAx * invA)
    dAinvy = (-invA * dAy * invA)
    dAinvz = (-invA * dAz * invA)

    Pc = CalcularMp(c, m, Expo)

    dpx = DerivadaPx(c, m, Expo)
    dpy = DerivadaPy(c, m, Expo)
    dpz = DerivadaPz(c, m, Expo)

    invAdBx = invA * dBx
    invAdBy = invA * dBy
    invAdBz = invA * dBz

    dAinvxB = dAinvx * B
    dAinvyB = dAinvy * B
    dAinvzB = dAinvz * B

    dphyx = (np.transpose(dpx) * invAB) + Pc * (invAdBx + dAinvxB)
    dphyy = (np.transpose(dpy) * invAB) + Pc * (invAdBy + dAinvyB)
    dphyz = (np.transpose(dpz) * invAB) + Pc * (invAdBz + dAinvzB)

    prox = np.array(dphyx)
    proy = np.array(dphyy)
    proz = np.array(dphyz)
    return prox, proy, proz


def SM(prox, proy, proz):
    n = max(prox.shape)
    B = np.zeros((6, 3 * n))

    B[0][range(0, 3 * n, 3)] = prox[0][:]
    B[1][range(1, 3 * n, 3)] = proy[0][:]

    B[2][range(2, 3 * n, 3)] = proz[0][:]
    B[3][range(1, 3 * n, 3)] = proz[0][:]
    B[3][range(2, 3 * n, 3)] = proy[0][:]
    B[4][range(0, 3 * n, 3)] = proz[0][:]
    B[4][range(2, 3 * n, 3)] = prox[0][:]
    B[5][range(0, 3 * n, 3)] = proy[0][:]
    B[5][range(1, 3 * n, 3)] = prox[0][:]
    return (B)


# 2) Armado de matriz k, funcion dentro de la integral
def integrandok(c, Dmax, m, Expo, ListaDePuntos):
    [Orden, dis] = Dsoporte(c, Dmax, ListaDePuntos)
    DominioS = np.transpose(ListaDePuntos[Orden])
    [prox, proy, proz] = Coeficientesa(DominioS, c, dis, Dmax, m, Expo)
    B = SM(prox, proy, proz)
    return B, Orden
