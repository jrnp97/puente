""" Module to define GLine formula """

import dask.array as da
"""
def Glinea(SR, nf, N, Dmax, m, Expo, ListaDePuntos):
    # 3) Integrales de linea
    sol = 0
    T = len(nf)
    for i in range(T):
        c = nf[i, 0:3]
        x = c[0]
        y = c[1]
        z = c[2]
        ur, bb2 = SR(x, y, z)
        [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
        phiM = Ensamblarphi(phi, Orden, N)
        sol += nf[i, 3] * np.transpose(phiM).dot(bb2)
    return sol
"""
