import asyncio


async def factorial(name, number):
    f = 1
    await asyncio.sleep(2)
    for i in range(2, number + 1):
        print(f"Task {name}: Compute factorial({i})...")
        await asyncio.sleep(1)
        f *= i
    print(f"Task {name}: factorial({number}) = {f}")


async def main():
    # Schedule three calls *concurrently*:
    asyncio.create_task(factorial('A', 2)).add_done_callback(lambda res: print('Finish'))
    print('A')


asyncio.run(main())
print('XD')