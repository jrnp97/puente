import os
import tempfile
from unittest import TestCase

import numpy as np

from TC3D import BASE_DIR
from TC3D import get_config
from TC3D import ptosdegaussd
from TC3D import ptosdegaussf

DATA_DIR = os.path.join(BASE_DIR, 'data')
config = get_config('config_test.ini')


class GaussPointCalculus(TestCase):
    """ Class to test gauss point functions """

    def test_ptosdegaussd(self):
        """ Test ptosdegaussd function result with file for specific parameters """
        result = ptosdegaussd(
            longitud_x=config['longitud'].getfloat('X'),
            longitud_y=config['longitud'].getfloat('Y'),
            longitud_z=config['longitud'].getfloat('Z'),
            numd_gx=config['num_d'].getint('GX'),
            numd_gy=config['num_d'].getint('GY'),
            numd_gz=config['num_d'].getint('GZ'),
        )
        expected_result = np.load(os.path.join('data', 'gauss_point_domain.npy'))
        comparison = result == expected_result
        self.assertTrue(comparison.all())

    def test_ptosdegaussf(self):
        """ Test ptosdegaussf function result with file for specific parameters """
        result = ptosdegaussf(
            longitud_x=config['longitud'].getfloat('X'),
            longitud_y=config['longitud'].getfloat('Y'),
            longitud_z=config['longitud'].getfloat('Z'),
            numd_fx=config['num_d'].getint('FX'),
            numd_fy=config['num_d'].getint('FY'),
        )
        expected_result = np.load(os.path.join('data', 'gauss_point_border_force.npy'))
        comparison = result == expected_result
        self.assertTrue(comparison.all())

    def test_Glinea(self):
        """ Test GLinea function result with file for specific parameters """
        # result = Glinea(SR, nff, N, Dmax, m, Expo, ListaDePuntos, bb)
        expected_result = np.load(os.path.join('data', 'f_glinea_one.npy'))
        # comparison = result == expected_result
        # self.assertTrue(comparison.all())
