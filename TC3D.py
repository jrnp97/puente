# -*- coding: utf-8 -*-
"""
Main Module to start program
"""
import os
import multiprocessing
from datetime import datetime
from configparser import ConfigParser

import h5py
import progressbar
import numpy as np

from raices import wei
from raices import raiz

from utils import variable_change

from tasks import pre_calculus

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(BASE_DIR, 'data')


def get_config(filename: str = 'config.ini') -> ConfigParser:
    """ Function para cargar variables y constantes """
    parser = ConfigParser()
    parser.read(os.path.join(BASE_DIR, filename))
    return parser


def lista(yes: np.array, equis: np.array, zetas: np.array) -> np.array:
    """
    Funcion para calculo de nodos de dominio
    # 2.1) CÁLCULO DE DISCRETIZACIÓN DEL DOMINIO: Nodos en el dominio
    Args:
        yes: Un Vector-Fila de (NumeroDivisionesX, 1), type: np.ndarray
        equis: Un Vector-Fila de (NumeroDivisionesY, 1), type: np.ndarray
        zetas: Un Vector-Fila de (NumeroDivisionesZ, 1), type: np.ndarray
    Returns:
        Una matriz (NumeroDivisionesX*NumeroDivisionesY*NumeroDivisionesZ, 1), type np.ndarray
    """
    [y, x, z] = np.meshgrid(yes, equis, zetas)
    # x, y and z are matrices with (NumeroDivisionesX, NumeroDivisionesY, NumeroDivisionesZ) dimensions.
    # now compact each matrix to one vector with (NumeroDivisionesX*NumeroDivisionesY*NumeroDivisionesZ, 1) dimensions.
    x = x.ravel()
    y = y.ravel()
    z = z.ravel()
    # generate one unique matrix with
    return np.transpose(np.concatenate([[x], [y], [z]]))


def ptosdegaussd(
        longitud_x: float, longitud_y: float, longitud_z: float, numd_gx: int, numd_gy: int, numd_gz: int
) -> np.array:
    """
    Function para calcular puntos de gauss en el dominio.
    # 2.2)  CÁLCULO DE DISCRETIZACIÓN DEL DOMINIO: ptos de Gauss
    Args:
        longitud_x: Un float-python describiendo TODO <para insertar descripcion>.
        longitud_y: Un float-python describiendo TODO <para insertar descripcion>.
        longitud_z: Un float-python describiendo TODO <para insertar descripcion>.
        numd_gx: Un int-python describiendo TODO <para insertar descripcion>.
        numd_gy: Un int-python describiendo TODO <para insertar descripcion>.
        numd_gz: Un int-python describiendo TODO <para insertar descripcion>.

    Returns:

    """
    xx = np.linspace(0, longitud_x, numd_gx)
    yy = np.linspace(0, longitud_y, numd_gy)
    zz = np.linspace(0, longitud_z, numd_gz)

    result = np.matrix([0, 0, 0, 0], dtype=float)
    GL = raiz()
    Wg = wei()
    wx, wy, wz = np.meshgrid(Wg, Wg, Wg)
    w_ravel = (wx * wy * wz).ravel()
    results = []
    for i in range(numd_gx - 1):
        gl1, ds1 = variable_change(
            a=xx[i],
            b=xx[i + 1],
            x=GL
        )
        for j in range(numd_gy - 1):
            gl2, ds2 = variable_change(
                a=yy[j],
                b=yy[j + 1],
                x=GL
            )
            for k in range(numd_gz - 1):
                gl3, ds3 = variable_change(
                    a=zz[k],
                    b=zz[k + 1],
                    x=GL
                )
                jacobian = ds1 * ds2 * ds3 * w_ravel
                x, y, z = np.meshgrid(gl1, gl2, gl3)
                x = x.ravel()
                y = y.ravel()
                z = z.ravel()

                Pgd = np.transpose(
                    np.concatenate([
                        [x],
                        [y],
                        [z],
                        [jacobian]
                    ],
                        0
                    )
                )
                results.append(Pgd)

    result = np.concatenate((result, *results), axis=0)  # improvement
    result = np.delete(result, 0, 0)
    result = np.array(result)
    return result


def ptosdegaussf(longitud_x: float, longitud_y: float, longitud_z: float, numd_fx: int, numd_fy: int) -> np.array:
    """
    Funcion para calcular puntos de gauss en la frontera de la fuera
    # 2.4) CÁLCULO DEL DOMINIO: Nodos en la frontera de Neumman ( Fuerzas)
    Args:
        longitud_x: Un float-python describiendo TODO <para insertar descripcion>.
        longitud_y: Un float-python describiendo TODO <para insertar descripcion>.
        longitud_z: Un float-python describiendo TODO <para insertar descripcion>.
        numd_fx: Un int-python describiendo TODO <para insertar descripcion>.
        numd_fy: Un int-python describiendo TODO <para insertar descripcion>.

    Returns:

    """
    xx = np.linspace(0, longitud_x, numd_fx)
    yy = np.linspace(0, longitud_y, numd_fy)
    PgD = np.matrix(([0, 0, 0, 0]), dtype=float)
    x = 0
    GL = raiz()
    Wg = wei()
    wy, wx = np.meshgrid(Wg, Wg)
    w_ravel = (wx * wy).ravel()
    for i in range(numd_fx - 1):
        x1 = xx[i]
        x2 = xx[i + 1]
        [GL2, ds2] = variable_change(x1, x2, GL)
        for j in range(numd_fy - 1):
            y1 = yy[j]
            y2 = yy[j + 1]
            [GL3, ds3] = variable_change(y1, y2, GL)

            y, x = np.meshgrid(GL3, GL2)
            x = x.ravel()
            y = y.ravel()
            z = y * 0
            jacobian = ds2 * ds3 * w_ravel
            Pgd = np.transpose(np.concatenate([[x], [y], [z], [jacobian]], 0))
            PgD = np.concatenate((PgD, Pgd), axis=0)

    PgD = np.delete(PgD, 0, 0)
    PgD[:, 2] = longitud_z
    PgD = np.array(PgD)
    return PgD


# ___________________________________________________________________________________________________
# 2.3) CÁLCULO DEL DOMINIO: Nodos en la frontera de Dirichlet ( Desplazamientos)
def evaluarnodos(ListaDePuntos, b0, zetas):
    Jposy = []
    Jposz = []
    JPP = []
    JPP2 = []

    zp = round(ListaDePuntos[0][2], 6)
    b0 = round(b0, 6)

    for i in range(N):
        x = round(ListaDePuntos[i][0], 6)
        z = round(ListaDePuntos[i][2], 6)
        y = round(ListaDePuntos[i][1], 6)
        if (y == b0):  # and (z == zp):
            Jposy.append(i)

    yp = round(ListaDePuntos[0][2], 6)
    for i in range(N):
        x = round(ListaDePuntos[i][0], 6)
        y = round(ListaDePuntos[i][1], 6)
        if (x == b0) and (y == yp):
            Jposz.append(i)

    for i in range(N):
        x = round(ListaDePuntos[i][0], 6)
        if x == b0:
            JPP.append(i)

    for i in range(N):
        z = round(ListaDePuntos[i][2], 6)
        if z == round(min(abs(zetas)), 6):
            JPP2.append(i)
    return Jposy, Jposz, JPP, JPP2


def main_calculus(output_file):
    config = get_config()
    # 2.1.1) CÁLCULO DE DISCRETIZACIÓN DEL DOMINIO: Nodos en el dominio

    longitudx = config['longitud'].getfloat('X')
    longitudy = config['longitud'].getfloat('Y')
    longitudz = config['longitud'].getfloat('Z')

    NumeroDivisionesX = config['numero_divisiones'].getint('X')
    NumeroDivisionesY = config['numero_divisiones'].getint('Y')
    NumeroDivisionesZ = config['numero_divisiones'].getint('Z')

    NumDgx = config['num_d'].getint('GX')
    NumDgy = config['num_d'].getint('GY')
    NumDgz = config['num_d'].getint('GZ')

    NumDfx = config['num_d'].getint('FX')
    NumDfy = config['num_d'].getint('FY')

    Dsopor = config['constantes'].getfloat('dominio_soporte')

    equis = np.linspace(0, longitudx, NumeroDivisionesX)
    yes = np.linspace(0, longitudy, NumeroDivisionesY)
    zetas = np.linspace(0, longitudz, NumeroDivisionesZ)

    ExponentesX = np.array([0, 1, 0, 0])
    ExponentesY = np.array([0, 0, 1, 0])
    ExponentesZ = np.array([0, 0, 0, 1])

    v_fuerzas_externas = np.array([0, -45e6, 0])

    D1 = config['constantes'].getfloat('E') * (1 - config['constantes'].getfloat('nu')) / (
            (1 - 2 * config['constantes'].getfloat('nu')) * (1 + config['constantes'].getfloat('nu')))
    D2 = config['constantes'].getfloat('E') * config['constantes'].getfloat('nu') / (
            (1 - 2 * config['constantes'].getfloat('nu')) * (1 + config['constantes'].getfloat('nu')))
    G = (D1 - D2) / 2
    D = np.asmatrix(
        [
            [D1, D2, D2, 0, 0, 0],
            [D2, D1, D2, 0, 0, 0],
            [D2, D2, D1, 0, 0, 0],
            [0, 0, 0, G, 0, 0],
            [0, 0, 0, 0, G, 0],
            [0, 0, 0, 0, 0, G]
        ]
    )

    # _________________________________________________________________________________________________
    # 1.2) DATOS DE ENTRADA: Propiedades propias de las MLS
    Expo = np.array([ExponentesX, ExponentesY, ExponentesZ])
    m = ExponentesX.shape[0]

    # _________________________________________________________________________________________________
    # _________________________________________________________________________________________________
    # 2.0) CÁLCULO DE DISCRETIZACION DEL DOMINIO

    Imo = (1 / 12) * config['longitud'].getfloat('Y') ** 3

    Dmax = Dsopor * abs(np.array([
        abs(equis[1]) - abs(equis[0]),  # deltax
        abs(yes[1]) - abs(yes[0]),  # deltay
        abs(zetas[1]) - abs(zetas[0]),  # deltaz
    ]))

    Num = np.array([
        [NumeroDivisionesX, NumeroDivisionesY, NumeroDivisionesZ],
        [NumDgx, NumDgy, NumDgz],
        [NumDfx, NumDfy, 0],
        [Dsopor, 0, 0]
    ])

    ListaDePuntos = lista(
        yes=yes,
        equis=equis,
        zetas=zetas,
    )
    N = ListaDePuntos.shape[0]

    PgD = ptosdegaussd(
        longitud_x=longitudx,
        longitud_y=longitudy,
        longitud_z=longitudz,
        numd_gx=NumDgx,
        numd_gy=NumDgy,
        numd_gz=NumDgz,
    )

    nff = ptosdegaussf(
        longitud_x=longitudx,
        longitud_y=longitudy,
        longitud_z=longitudz,
        numd_fx=NumDfx,
        numd_fy=NumDfy,
    )
    # Ptos de Gauss en la frontera del empotramiento
    nff[:, 2] = config['longitud']['Z']
    nfp = ptosdegaussf(
        longitud_x=longitudx,
        longitud_y=longitudy,
        longitud_z=longitudz,
        numd_fx=NumDfx,
        numd_fy=NumDfy,
    )
    nfp[:, 2] = 0

    with h5py.File(output_file, 'w') as file_:
        file_.create_dataset(name='p_gauss_dominio', data=PgD)
        file_.create_dataset(name='p_gauss_ff', data=nff)
        file_.create_dataset(name='p_gauss_fp', data=nfp)
        file_.create_dataset(name='ListaDePuntos', data=ListaDePuntos)
        file_.create_dataset(name='fuerzas_externas', data=v_fuerzas_externas)
        file_.create_dataset(name='Expo', data=Expo)
        file_.create_dataset(name='Dmax', data=Dmax)
        file_.create_dataset(name='D', data=D)


def main(data_file, pid_file):
    try:
        with h5py.File(data_file, 'r') as file_:
            total_nfp = file_['p_gauss_fp'].shape[0]
            total_nff = file_['p_gauss_ff'].shape[0]
            total_pgd = file_['p_gauss_dominio'].shape[0]
            listadepuntos = file_['ListaDePuntos'][()]
            v_fuerzas_externas = file_['fuerzas_externas'][()]
            expo = file_['Expo'][()]
            dmax = file_['Dmax'][()]
            d = file_['D'][()]
            data_set_nff = file_['p_gauss_ff']
            data_set_nfp = file_['p_gauss_fp']
            data_set_pgd = file_['p_gauss_dominio']
            totals = [total_nff, total_nfp, total_pgd]
            total_jobs = sum(totals)
            for index in range(total_jobs):
                pre_calculus.delay(
                    nff_matrix=np.reshape(data_set_nff[index, 0:4], (1, 4)) if index < total_nff else None,
                    nfp_matrix=np.reshape(data_set_nfp[index, 0:4], (1, 4)) if index < total_nfp else None,
                    pdg_matrix=np.reshape(data_set_pgd[index, 0:4], (1, 4)) if index < total_pgd else None,
                    dmax=dmax,
                    expo=expo,
                    bb=v_fuerzas_externas,
                    listadepuntos=listadepuntos,
                    d=d,
                )
    finally:
        if os.path.exists(pid_file):
            os.remove(pid_file)


def monitor(celery_app, progress):
    state = celery_app.events.State()

    def on_success(event):
        state.event(event)
        # task name is sent only with -received event, and state
        # will keep track of this for us.
        task = state.tasks.get(event['uuid'])
        if task.name == pre_calculus.name:
            progress.update(progress.value + 1)

    with celery_app.connection() as connection:
        recv = celery_app.events.Receiver(connection, handlers={
            'task-succeeded': on_success,
            '*': state.event,
        })
        recv.capture(limit=None, timeout=None, wakeup=True)


if __name__ == "__main__":
    result_file = 'test_file'
    info_file = os.path.join(DATA_DIR, f'{result_file}_data.hdf5')
    file_name = f'{result_file}_{datetime.now().strftime("%Y%m%d%H%M%S")}.hdf5'
    pid_file = os.path.join(BASE_DIR, 'process.lock')
    # main_calculus(info_file)

    with h5py.File(info_file, 'r') as file_:
        total_nfp = file_['p_gauss_fp'].shape[0]
        total_nff = file_['p_gauss_ff'].shape[0]
        total_pgd = file_['p_gauss_dominio'].shape[0]
        totals = sum([total_nff, total_nfp, total_pgd])
    pid_process = None
    if os.path.exists(pid_file):
        pid_process = open(pid_file, 'r').read().isdigit()

    if not pid_process:
        process_ = multiprocessing.Process(
            target=main,
            args=(info_file, pid_file),
            daemon=True,
        )
        process_.start()
        with open(pid_file, 'w') as file_:
            file_.write(str(process_.pid))

    from tasks import app
    progress = progressbar.ProgressBar(prefix='Executed Jobs: ', max_value=totals)
    monitor(app, progress=progress)


    """
    from ELASEFG3D import *

    f = Glinea(SR, nff, N, Dmax, m, Expo, ListaDePuntos) # Cálculo condición de borde natural (F)

    [KA, FA] = GLlinea3(nfp, N, Dmax, m, Expo, ListaDePuntos, SR) # Calculo condicion de borde esencial (u)

    K2 = CalculoK(PgD, D, N, Dmax, m, Expo, ListaDePuntos) # Cálculo K

    SOL = np.linalg.solve(K2 - KA, f - FA)  # Solución sistema lineal
    SOL = np.array(SOL)

    # ______________________________________________________________________________________________________
    ''' Posprocesamiento'''

    nx = os.path.join(BASE_DIR, 'nodos_nx.txt')
    nx = np.loadtxt(nx)
    ListaDePuntos2 = nx / 1000
    N = max(np.shape(ListaDePuntos2))
    [u, Sx, Sy, Sz, Tyz, Txz, Txy] = posprocesamiento(SOL, D, N, Dmax, m, Expo, ListaDePuntos, ListaDePuntos2)

    Resultado = np.dstack((Sx, Sy, Sz, Txy, Txz, Tyz))
    Resultado = np.matrix(Resultado)
    """
    # np.savetxt("Prop.txt", Num)
    # np.savetxt("Ptos.txt", ListaDePuntos)
    # np.savetxt("Esf.txt", Resultado)
    # np.savetxt("u.txt", u)
