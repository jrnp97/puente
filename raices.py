# Code name : Raices y pesos de gauss con 8 puntos de gauss
# Topic: Metodos numericos - Sol a integrales por cuadratura
# Author: Jose A. Martines T.

import numpy as np


def raiz():
    """ Function to return a Vector Column-Vector with 8 x 1 """
    return np.array([
        [0.1834346424956498049394761],
        [0.5255324099163289858177390],
        [0.7966664774136267395915539],
        [0.9602898564975362316835609],
        [-0.1834346424956498049394761],
        [-0.5255324099163289858177390],
        [-0.7966664774136267395915539],
        [-0.9602898564975362316835609]
    ])


def wei():
    """ Function to return a Vector Column-Vector with 8 x 1 """
    return np.array([
        [0.3626837833783619829651504],
        [0.3137066458778872873379622],
        [0.2223810344533744705443560],
        [0.1012285362903762591525314],
        [0.3626837833783619829651504],
        [0.3137066458778872873379622],
        [0.2223810344533744705443560],
        [0.1012285362903762591525314]
    ])


W = np.matrix(wei(), dtype=float)
