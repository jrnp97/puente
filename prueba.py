import numpy as np 




u=np.loadtxt('u.txt')*10
nx=np.loadtxt('des_total.txt')
#print(u)
N=max(np.shape(nx))

for i in range (N):
    '''error en el eje x'''
    if i==0:
        if abs(nx[i][4])<1e-2:
            error=abs(u[i][0])
        else:
          error=abs((nx[i][4]-u[i][0])/nx[i][4])
    else:
        if abs(nx[i][4])<1e-2:
            error1=abs(u[i][0])
        else:
          error1=abs((nx[i][4]-u[i][0])/nx[i][4])
        if error1>error:
           error=error1 
           d=i
             
 



print('el error maximo es ={}'.format(error))
print('poscion del valor maximo',d)
print('valor de nx=',nx[d][4])
print('valor del codigo',u[d][0])

print('valor maximo nx=',max(abs(nx[:,4])))
print('valor min nx=',min(abs(nx[:,4])))
print('valor maximo u=',max(abs(u[:,0])))
print('valor min u=',min(abs(u[:,0])))