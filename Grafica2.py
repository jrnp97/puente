from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
#https://stackoverflow.com/questions/14995610/how-to-make-a-4d-plot-with-matplotlib-using-arbitrary-data


#Path="P2Grid"


ptos = np.loadtxt("nodos_nx.txt")*1e3
u    = np.loadtxt("u.txt")*1e3
Esf  = np.loadtxt("Esf.txt")/1e6
#Sy   = np.loadtxt(Path+"/Sy.txt")/1e6
#Sz   = np.loadtxt(Path+"/Sz.txt")/1e6


d=np.stack((u[:,0],u[:,1],u[:,2],Esf[:,0],Esf[:,1],Esf[:,2]), axis=-1)
fig = plt.figure()
title=["Displacement u","Displacement v","Displacement w","Stress $\\sigma_{x}$","Stress $\\sigma_{y}$","Stress $\\sigma_{z}$"]
ctitle=["u (mm)","v (mm)","w (mm)","$\\sigma_{x}$ (MPa)","$\\sigma_{y}$ (MPa)","$\\sigma_{z}$ (MPa)"]

for i in range(6):
 a=231+i

 ax1 = fig.add_subplot(a, projection='3d')
 img1 = ax1.scatter(ptos[:,0], ptos[:,1]+10, ptos[:,2], c=d[:,i],s=60, cmap=plt.jet())
 ax1.set_title(title[i])
 cbar = fig.colorbar(img1)
 cbar.set_label(ctitle[i],rotation=0)
 #ax1.colorbar.set_label("lol")

plt.show()

