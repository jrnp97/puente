# -*- coding: utf-8 -*-
import math

import multiprocessing

import numpy as np
from numpy.linalg import inv

from raices import wei
from raices import raiz

# from scipy import sparse

GL = np.array(raiz(), dtype=float)
Wg = np.array(wei(), dtype=float)


# _______________________________________________________________________________________________________________________
# _________________________________________________Módulos de funciones__________________________________________________
# ___________________________D____________________________________________________________________________________________
# _______________________________________________________________________________________________________________________
# __________________________________MÓDULO 1: "FunMLS". Funciones propias del MLS________________________________________

# 1) Evaluar # de pts en el dominio de soporte
def Dsoporte(c, Dmax, ListaDePuntos):
    disx = []
    disy = []
    disz = []
    orden = []

    N = ListaDePuntos.shape
    pun2 = np.zeros(N)
    dif2 = np.zeros(N)

    pun2[:][:, 0] = c[0]
    pun2[:][:, 1] = c[1]
    pun2[:][:, 2] = c[2]

    dif = abs(pun2 - ListaDePuntos)
    dif3 = (pun2 - ListaDePuntos)

    dif2[:][:, 0] = dif[:][:, 0] - Dmax[0] * np.ones(N[0])
    dif2[:][:, 1] = dif[:][:, 1] - Dmax[1] * np.ones(N[0])
    dif2[:][:, 2] = dif[:][:, 2] - Dmax[2] * np.ones(N[0])

    for i in range(N[0]):
        if (dif2[i][0] <= 0.000001) and (dif2[i][1] <= 0.000001) and (dif2[i][2] <= 0.000001):
            disx.append(dif3[i][0])
            disy.append(dif3[i][1])
            disz.append(dif3[i][2])
            orden.append(i)
    orden = np.array(orden)
    dis = np.array([disx, disy, disz])
    return (orden, dis)


# _______________________________________________________________________________________________________________________
# 2) Función para calcular matriz P
def CalcularMp(DominioS, m, Expo):
    n = max(DominioS.shape)
    if n == 3:
        K = np.zeros((m), dtype=float)
        for j in range(m):
            K[j] = ((DominioS[0]) ** Expo[0, j]) * ((DominioS[1]) ** Expo[1, j]) * ((DominioS[2]) ** Expo[2, j])
    else:
        K = np.zeros((n, m), dtype=float)
        for i in range(n):
            x3 = DominioS[0][i]
            y3 = DominioS[1][i]
            z3 = DominioS[2][i]
            for j in range(m):
                K[i][j] = (x3 ** Expo[0, j]) * (y3 ** Expo[1, j]) * (z3 ** Expo[2, j])
    return (K)


# _______________________________________________________________________________________________________________________
# 3) Función para calcular matriz de pesos
def wf(DominioS, dis, Dmax):
    d_s = np.shape(DominioS)
    numnodos = max(d_s)
    Ww = np.zeros((numnodos, numnodos), dtype=float)
    Wx = np.zeros((numnodos, numnodos), dtype=float)
    Wy = np.zeros((numnodos, numnodos), dtype=float)
    Wz = np.zeros((numnodos, numnodos), dtype=float)
    for i in range(numnodos):
        drdx = np.sign(dis[0, i]) / Dmax[0]
        drdy = np.sign(dis[1, i]) / Dmax[1]
        drdz = np.sign(dis[2, i]) / Dmax[2]
        rx = abs(dis[0, i] / Dmax[0])
        ry = abs(dis[1, i] / Dmax[1])
        rz = abs(dis[2, i] / Dmax[2])
        if rx > 0.5:
            wx = (4 / 3) - 4 * rx + 4 * rx * rx - (4 / 3) * rx ** 3
            dwx = (-4 + 8 * rx - 4 * rx ** 2) * drdx
        elif rx <= 0.5:
            wx = (2 / 3) - 4 * rx * rx + 4 * rx ** 3
            dwx = (-8 * rx + 12 * rx ** 2) * drdx

        if ry > 0.5:
            wy = (4 / 3) - 4 * ry + 4 * ry ** 2 - (4 / 3) * ry ** 3
            dwy = (-4 + 8 * ry - 4 * ry ** 2) * drdy
        elif ry <= 0.5:
            wy = (2 / 3) - 4 * ry ** 2 + 4 * ry ** 3
            dwy = (-8 * ry + 12 * ry ** 2) * drdy

        if rz > 0.5:
            wz = (4 / 3) - 4 * rz + 4 * rz ** 2 - (4 / 3) * rz ** 3
            dwz = (-4 + 8 * rz - 4 * rz ** 2) * drdz
        elif rz <= 0.5:
            wz = (2 / 3) - 4 * rz ** 2 + 4 * rz ** 3
            dwz = (-8 * rz + 12 * rz ** 2) * drdz
        Ww[i][i] = wx * wy * wz
        Wx[i][i] = dwx * wy * wz
        Wy[i][i] = wx * dwy * wz
        Wz[i][i] = wx * wy * dwz

    WW = np.asmatrix(Ww)
    WX = np.asmatrix(Wx)
    WY = np.asmatrix(Wy)
    WZ = np.asmatrix(Wz)
    return (WW, WX, WY, WZ)


# _______________________________________________________________________________
# 4) DEREVIDAD DE P  EN X
def DerivadaPx(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp1 = np.absolute((Expo[0, i] - 1))
        dp1.append(Expo[0, i] * (c[0] ** (expdp1)) * (c[1] ** (Expo[1, i])) * (c[2] ** (Expo[2, i])))
    return (dp1)


# _______________________________________________________________________________
# 5) DEREVIDAD DE P  EN Y
def DerivadaPy(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp2 = abs(Expo[1, i] - 1)
        dp1.append(Expo[1, i] * (c[0] ** (Expo[0, i])) * (c[1] ** (expdp2)) * c[2] ** (Expo[2, i]))
    return (dp1)


# _______________________________________________________________________________
# 6) DEREVIDAD DE P  EN Z
def DerivadaPz(c, m, Expo):
    dp1 = []
    for i in range(m):
        expdp3 = abs(Expo[2, i] - 1)
        dp1.append(Expo[2, i] * (c[0] ** (Expo[0, i])) * (c[1] ** (Expo[1, i])) * (c[2] ** (expdp3)))
    return (dp1)


# _______________________________________________________________________________
# 7) Cálculo de coeficientes a
def Coeficientesa(DominioS, c, dis, Dmax, m, Expo):
    P = CalcularMp(DominioS, m, Expo)
    [W, Wx, Wy, Wz] = wf(DominioS, dis, Dmax)
    PP = np.matrix(P)
    A = np.transpose(PP) * W * PP
    dAx = np.transpose(PP) * Wx * PP
    dAy = np.transpose(PP) * Wy * PP
    dAz = np.transpose(PP) * Wz * PP

    B = np.transpose(PP) * W
    dBx = np.transpose(PP) * Wx
    dBy = np.transpose(PP) * Wy
    dBz = np.transpose(PP) * Wz

    invA = inv(A)
    invAB = invA * B
    dAinvx = (-invA * dAx * invA)
    dAinvy = (-invA * dAy * invA)
    dAinvz = (-invA * dAz * invA)

    Pc = CalcularMp(c, m, Expo)

    dpx = DerivadaPx(c, m, Expo)
    dpy = DerivadaPy(c, m, Expo)
    dpz = DerivadaPz(c, m, Expo)

    invAdBx = invA * dBx
    invAdBy = invA * dBy
    invAdBz = invA * dBz

    dAinvxB = dAinvx * B
    dAinvyB = dAinvy * B
    dAinvzB = dAinvz * B

    dphyx = (np.transpose(dpx) * invAB) + Pc * (invAdBx + dAinvxB)
    dphyy = (np.transpose(dpy) * invAB) + Pc * (invAdBy + dAinvyB)
    dphyz = (np.transpose(dpz) * invAB) + Pc * (invAdBz + dAinvzB)

    prox = np.array(dphyx)
    proy = np.array(dphyy)
    proz = np.array(dphyz)
    return (prox, proy, proz)


# __________________________________________________________________________________
# 8) Cálculo de funciones basicas del MLS (Sol para un pto, osea phi)
def PHI(c, Dmax, m, Expo, ListaDePuntos):
    [Orden, dis] = Dsoporte(c, Dmax, ListaDePuntos)
    DominioS = np.transpose(ListaDePuntos[Orden])
    P = CalcularMp(DominioS, m, Expo)
    (W, Wx, Wy, Wz) = wf(DominioS, dis, Dmax)
    Pt = np.transpose(np.asmatrix(P))
    Pc = CalcularMp(c, m, Expo)
    M = Pt * W * P
    B = Pt * W
    A = inv(M) * (B)
    phi = np.asarray(Pc * A)
    numDS = len(Orden)
    return (phi, numDS, Orden)


# ______________________________________________________FIN MÓDULO FunMLS________________________________________________
# _______________________________________________________________________________________________________________________
# __________________________________MÓDULO 2: "Funelas". Funciones de elasticidad________________________________________
# 1) Ensamblar la matriz Phi
def Ensamblarphi(phi, Orden, N):
    PHIs = np.zeros((3, 3 * N), dtype=float)
    PHIs[0][3 * Orden] = phi
    PHIs[1][3 * Orden + 1] = phi
    PHIs[2][3 * Orden + 2] = phi
    return (PHIs)


# _______________________________________________________________________________________________________________________
# 2) Armado de strain matrix
def SM(prox, proy, proz):
    n = max(prox.shape)
    B = np.zeros((6, 3 * n))

    B[0][range(0, 3 * n, 3)] = prox[0][:]
    B[1][range(1, 3 * n, 3)] = proy[0][:]

    B[2][range(2, 3 * n, 3)] = proz[0][:]
    B[3][range(1, 3 * n, 3)] = proz[0][:]
    B[3][range(2, 3 * n, 3)] = proy[0][:]
    B[4][range(0, 3 * n, 3)] = proz[0][:]
    B[4][range(2, 3 * n, 3)] = prox[0][:]
    B[5][range(0, 3 * n, 3)] = proy[0][:]
    B[5][range(1, 3 * n, 3)] = prox[0][:]
    return (B)


# _______________________________________________________________________________
# 3) Calculo de la matriz K
def K_integral(PgD, D, Dmax, m, Expo, ListaDePuntos, i, K):
    """
    This function perform Integral of one point from K Matrix.
    """
    c = PgD[i][0:3]
    jacobian = PgD[i][3]
    [B5, ORDEN] = integrandok(c, Dmax, m, Expo, ListaDePuntos)
    L1 = len(ORDEN)

    en = np.zeros((3 * L1), dtype=int)
    ORDEN = np.array(ORDEN)

    en[range(0, 3 * L1, 3)] = 3 * ORDEN
    en[range(1, 3 * L1, 3)] = 3 * ORDEN + 1
    en[range(2, 3 * L1, 3)] = 3 * ORDEN + 2

    B5 = np.asmatrix(B5)
    B5t = np.transpose(B5)
    M_s = jacobian * np.linalg.multi_dot([B5t, D, B5])
    return tuple(en[np.mgrid[:3 * L1, :3 * L1]]), M_s


def send_to_pool(args):
    """ function adapter to allow call this with a poll of worker"""
    return K_integral(*args)


def CalculoK(PgD, D, N, Dmax, m, Expo, ListaDePuntos):
    print("caculo de la matriz K ha comenzado")

    NPdD = len(PgD)
    K = np.zeros((3 * N, 3 * N), dtype=float)
    # Here for optimize general calculation process is reserved 5 system workers of host,
    # for calculate 5 integral points at same time.
    with multiprocessing.Pool(processes=5) as pool:
        # Creating python generator with all points parameters required for calculate integral
        # generators are lazy and only are executed when it is necessary.
        gdata = ([PgD, D, Dmax, m, Expo, ListaDePuntos, i, K] for i in range(NPdD))
        # Sending points to pool and give to workers to process it's.
        result = pool.map(send_to_pool, gdata)
        result_ = {}
        '''for data in result:
            key, value = data
            K[key] += value '''
    return K  # return K result (jose: valida)


# ______________________________________________________FIN MÓDULO Funelas_______________________________________________

# _______________________________________________________________________________________________________________________
# __________________________________MÓDULO 3: "Funinte". Funciones de integración________________________________________
# 1) Cambio de variables
def cv(a, b, x):
    """
    Function to
    Args:
        a: A float
        b: A float
        x: A np.array

    Returns:

    """
    y = (b - a) * x / 2 + (a + b) / 2
    dy = abs(b - a) / 2
    return y, dy


# _______________________________________________________________________________________________________________________
# 2) Armado de matriz k, funcion dentro de la integral
def integrandok(c, Dmax, m, Expo, ListaDePuntos):
    [Orden, dis] = Dsoporte(c, Dmax, ListaDePuntos)
    DominioS = np.transpose(ListaDePuntos[Orden])
    [prox, proy, proz] = Coeficientesa(DominioS, c, dis, Dmax, m, Expo)
    B = SM(prox, proy, proz)
    return (B, Orden)


# _______________________________________________________________________________________________________________________
# 3) Integrales de linea
'''
def Calculo_Glinea(SR, nf, N, Dmax, m, Expo, ListaDePuntos,i,T,sol):
    c = nf[i, 0:3]
    x = c[0]
    y = c[1]
    z = c[2]
    ur, bb2 = SR(x, y, z)
    [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
    phiM = Ensamblarphi(phi, Orden, N)
    return(nf[i, 3] * np.transpose(phiM).dot(bb2))
    
    

def send_to_pool2(args1):
    return(Calculo_Glinea(*args1))

def Glinea(SR, nf, N, Dmax, m, Expo, ListaDePuntos):
    sol = 0
    T = len(nf)
    with multiprocessing.Pool(processes=5) as pool:
        
        gdata2 = ([SR, nf, N, Dmax, m, Expo, ListaDePuntos,i,T,sol] for i in range(T))
        result= pool.map(send_to_pool2, gdata2)
        sol2=np.sum(result,axis=0)
    return(sol2)'''


def Glinea(SR, nf, N, Dmax, m, Expo, ListaDePuntos, bb):
    """
    Function to calculate F
    """
    sol = 0
    T = len(nf)
    for i in range(T):
        c = nf[i, 0:3]
        x = c[0]
        y = c[1]
        z = c[2]
        ur, bb2 = SR(x, y, z, bb)
        phi, _, Orden = PHI(c, Dmax, m, Expo, ListaDePuntos)
        phiM = Ensamblarphi(phi, Orden, N)
        sol += nf[i, 3] * np.transpose(phiM).dot(bb2)
        print("Calculo de F en un:", round(100 * i / T, 1), "%")
    return sol


# _______________________________________________________________________________________________________________________
# 4) Penalización

def Calculo_GLlinea31(nfp, N, Dmax, m, Expo, ListaDePuntos, SR, Ka, i, bb):
    c = nfp[i, 0:3]
    x = c[0]
    y = c[1]
    z = c[2]
    [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
    Phi = Ensamblarphi(phi, Orden, N)
    alfaa = np.max(phi) * 10 ** 15
    # Ka += nfp[i, 3] * np.transpose(Phi).dot(Phi) * alfaa
    return nfp[i, 3] * np.transpose(Phi).dot(Phi) * alfaa
    # return (Ka)


def Calculo_GLlinea32(nfp, N, Dmax, m, Expo, ListaDePuntos, SR, Fa, i, bb):
    c = nfp[i, 0:3]
    x = c[0]
    y = c[1]
    z = c[2]
    [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
    Phi = Ensamblarphi(phi, Orden, N)
    alfaa = np.max(phi) * 10 ** 15
    ur, bb = SR(x, y, z, bb)
    # Fa += nfp[i, 3] * np.transpose(Phi).dot(ur[:, None]) * alfaa
    return nfp[i, 3] * np.transpose(Phi).dot(ur[:, None]) * alfaa
    # return (Fa)


def send_to_pool31(args3):
    return (Calculo_GLlinea31(*args3))


def send_to_pool32(args3):
    return (Calculo_GLlinea32(*args3))


def GLlinea3(nfp, N, Dmax, m, Expo, ListaDePuntos, SR, bb):
    print("calculo de FA y Ka ha comenzado")

    total = len(nfp)
    print(len(nfp))
    Ka = np.zeros([N * 3, N * 3])
    Fa = np.zeros([3 * N, 1])

    with multiprocessing.Pool(processes=5) as pool:
        gdata3 = ([nfp, N, Dmax, m, Expo, ListaDePuntos, SR, Ka, i, bb] for i in range(total))
        result = pool.map(send_to_pool31, gdata3)
        KA2 = np.sum(result, axis=0)

    with multiprocessing.Pool(processes=5)as pool:
        gdata4 = ([nfp, N, Dmax, m, Expo, ListaDePuntos, SR, Fa, i, bb] for i in range(total))
        result = pool.map(send_to_pool32, gdata4)
        FA2 = np.sum(result, axis=0)

    return (KA2, FA2)


# _______________________________________________________________________________________________________________________
# _______________________________________________________________________________________________________________________
# _________________________________________MÓDULO 4: Posprocesamiento____________________________________________________


def posprocesamiento(SOL, D, N, Dmax, m, Expo, ListaDePuntos, ListaDePuntos2):
    u = []
    sigmax = []
    sigmay = []
    sigmaz = []
    tauyz = []
    tauxz = []
    tauxy = []
    print("posprocesamiento ha comenzado")

    for i in range(N):
        c = ListaDePuntos2[i]
        sol = []
        [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
        [sigma, vicky] = integrandok(c, Dmax, m, Expo, ListaDePuntos)
        PHIM = SM(phi, phi, phi)
        PHIM = np.delete(PHIM, (3, 4, 5), 0)

        sol = np.zeros((3 * numDS), dtype=float)
        sol[range(0, 3 * numDS, 3)] = SOL[3 * Orden, 0]
        sol[range(1, 3 * numDS, 3)] = SOL[3 * Orden + 1, 0]
        sol[range(2, 3 * numDS, 3)] = SOL[3 * Orden + 2, 0]

        u.append(np.transpose(PHIM.dot(sol)))
        esf = np.linalg.multi_dot([D, sigma, sol])
        esf = np.array(esf)
        print("posprocesamiento en un:", round(100 * i / N, 1), "%")

        sigmax.append(esf[0][0])
        sigmay.append(esf[0][1])
        sigmaz.append(esf[0][2])
        tauyz.append(esf[0][3])
        tauxz.append(esf[0][4])
        tauxy.append(esf[0][5])
    sigmax = np.array(sigmax)
    sigmay = np.array(sigmay)
    sigmaz = np.array(sigmaz)
    tauxy = np.array(tauxy)
    tauxz = np.array(tauxz)
    tauyz = np.array(tauyz)
    u = np.array(u)
    return (u, sigmax, sigmay, sigmaz, tauyz, tauxz, tauxy)
# _______________________________________________________________________________________________________________________
# _______________________________________________________Fin?____________________________________________________________
