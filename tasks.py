from __future__ import absolute_import, unicode_literals

from celery import Celery

import numpy as np

from utils import SR, integrandok
from utils import PHI
from utils import Ensamblarphi

app = Celery(
    main='tasks',
    broker='amqp://localhost',
    # backend='db+sqlite:///results.sqlite',
)

app.conf.task_serializer = 'pickle'
app.conf.result_serializer = 'pickle'
app.conf.accept_content = ['pickle']
app.conf.result_accept_content = ['pickle']
app.conf.worker_send_task_events = True
app.conf.result_extended = True


def calculus_(
        matrix: np.array,
        dmax: np.array,
        expo: np.array,
        bb: np.array,
        listadepuntos: np.array,
) -> np.array:
    m = expo[0].shape[0]
    N = listadepuntos.shape[0]
    c = matrix[0, 0:3]
    x = c[0]
    y = c[1]
    z = c[2]
    ur, bb2 = SR(x, y, z, bb)
    [phi, numDS, Orden] = PHI(c, dmax, m, expo, listadepuntos)
    phiM = Ensamblarphi(phi, Orden, N)
    alfaa = np.max(phi) * 10 ** 15
    return matrix, ur, bb2, phi, numDS, Orden, phiM, alfaa


def calculus_pdg(
        matrix: np.array,
        dmax: np.array,
        expo: np.array,
        bb: np.array,
        listadepuntos: np.array,
        d: np.array,
) -> np.array:
    m = expo[0].shape[0]
    N = listadepuntos.shape[0]
    c = matrix[0, 0:3]
    jacobian = matrix[0][3]
    [B5, ORDEN] = integrandok(c, dmax, m, expo, listadepuntos)
    L1 = len(ORDEN)
    en = np.zeros((3 * L1), dtype=int)
    ORDEN = np.array(ORDEN)
    en[range(0, 3 * L1, 3)] = 3 * ORDEN
    en[range(1, 3 * L1, 3)] = 3 * ORDEN + 1
    en[range(2, 3 * L1, 3)] = 3 * ORDEN + 2
    B5 = np.asmatrix(B5)
    B5t = np.transpose(B5)
    M_s = jacobian * np.linalg.multi_dot([B5t, d, B5])
    point = tuple(en[np.mgrid[:3 * L1, :3 * L1]])
    return point, M_s


@app.task
def pre_calculus(
        nff_matrix: np.array,
        nfp_matrix: np.array,
        pdg_matrix: np.array,
        dmax: np.array,
        expo: np.array,
        bb: np.array,
        listadepuntos: np.array,
        d: np.array,
) -> bool:
    nff_result = None
    nfp_result = None
    pdg_result = None
    if nff_matrix is not None:
        nff_result = calculus_(
            nff_matrix,
            dmax,
            expo,
            bb,
            listadepuntos,
        )
        matrix, ur, bb2, phi, numDS, Orden, phiM, alfaa = nff_result
        glinea = matrix[0, 3] * np.transpose(phiM).dot(bb2)
        glinea_point_analysis.delay(glinea)

    if pdg_matrix is not None:
        point, M_s = calculus_pdg(
            pdg_matrix,
            dmax,
            expo,
            bb,
            listadepuntos,
            d,
        )
        k_point_calculus.delay(point, M_s)

    if nfp_matrix is not None:
        nfp_result = calculus_(
            nfp_matrix,
            dmax,
            expo,
            bb,
            listadepuntos,
        )
        matrix, ur, bb2, phi, numDS, Orden, phiM, alfaa = nfp_result
        fa = matrix[0, 3] * np.transpose(phiM).dot(ur[:, None]) * alfaa
        glinea3_fa_point_analysis.delay(fa)
        # https://github.com/numpy/numpy/issues/11517#issuecomment-403009722
        # ka = matrix[0, 3] * np.transpose(phiM.astype(np.float16)).dot(phiM.astype(np.float16)) * alfaa  # BIG Operation
        # glinea3_ka_point_analysis.delay(ka)

    return True


@app.task
def glinea_point_analysis(result: np.array) -> np.array:
    return result


@app.task
def glinea3_fa_point_analysis(result: np.array) -> np.array:
    return result


@app.task
def glinea3_ka_point_analysis(result: np.array) -> np.array:
    return result


@app.task
def k_point_calculus(point, result: np.array) -> np.array:
    return point, result


# @app.task
# def sum_results(point_results):
#     return np.sum(point_results)
