# TODO Delete <general first sequential implementation>
"""
# 4) Penalización
def GLlinea3(nfp, N, Dmax, m, Expo, ListaDePuntos, SR):
    total = len(nfp)
    Ka = np.zeros([N * 3, N * 3])
    Fa = np.zeros([3 * N, 1])
    for i in range(total):
        c = nfp[i, 0:3]
        x = c[0]
        y = c[1]
        z = c[2]
        [phi, numDS, Orden] = PHI(c, Dmax, m, Expo, ListaDePuntos)
        Phi = Ensamblarphi(phi, Orden, N)
        alfaa = np.max(phi) * 10 ** 15
        Ka += nfp[i, 3] * np.transpose(Phi).dot(Phi) * alfaa
        ur, bb = SR(x, y, z)
        Fa += nfp[i, 3] * np.transpose(Phi).dot(ur[:, None]) * alfaa
        print("Calculo de KA y FA en un:", round(100 * i / total, 1), "%")
    return (Ka, Fa)
"""